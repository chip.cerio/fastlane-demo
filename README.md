# Fastlane Sandbox

A sandbox project on how to automate builds and send it to Google Play automatically

## Why Fastlane?

* Automates uploading of apk/aab Google Play
* Automates publishing of metadata (short description, full description, etc.) to Google Play
* Generates screenshots needed for the new versions of the app

### Getting Started

When you try running this project, this will create a release build and upload it to Google Play Store on the Alpha stage. If you want to see it in action, send me your `email` and I will send you an invite. Here's the [app on Play Store](https://play.google.com/store/apps/details?id=com.chipcerio.fastlanedemo)

### Prerequisites

* set `ANDROID_HOME` environment variable to your Android SDK
* add it to your `PATH` as `$PATH:$ANDROID_HOME/tools`
* download Android SDK [command line tools](https://developer.android.com/studio#downloads)
* unzip the file. Make sure its the same path with `ANDROID_HOME`
* update the SDK Manager

```
${ANDROID_HOME}/tools/bin/sdkmanager --update
```

* lastly, clone the project

### Install Fastlane

Check out the Fastlane [docs](https://docs.fastlane.tools/)

### Build

`fastlane android build_release`

### Deploy to Google Play

`fastlane android alpha`


### Run Tests

`fastlane android test`

### Where to go from here?

This workflow lacks the CI setup. A good practice is to run first unit tests, ui tests, and then if all tests passed, run fastlane.

### Contribute/Suggestions

Send merge requests or file an issue

